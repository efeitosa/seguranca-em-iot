# Segurança em IoT

## 13/04

- [Cenário de Segurança](https://gitlab.com/efeitosa/seguranca-em-iot/-/blob/main/Aulas/Cenario_Seguranca.pdf): Apresentação do professor e visão geral do cenário de segurança
- [Conceitos de Segurança](https://gitlab.com/efeitosa/seguranca-em-iot/-/blob/main/Aulas/Conceitos_Seguranca.pdf): Apresentação dos princípios e conceitos da área de segurança da informação
- [Ameeaças e Ataques](https://gitlab.com/efeitosa/seguranca-em-iot/-/blob/main/Aulas/Ameac%CC%A7as_e_Ataques.pdf): Apresentação das principais ameaças e ataques aos dispositivos IoT
- [OWASP IoT Top10](https://gitlab.com/efeitosa/seguranca-em-iot/-/blob/main/Aulas/OWASP_IoT.pdf): Apresentação do projeto OWASP para segurança em IoT

## 20/04

- [Man in the Middle](https://gitlab.com/efeitosa/seguranca-em-iot/-/blob/main/Aulas/mitm.pdf): Apresentação sobre Man in The Middle e suas repercurssões
- [Análise de Firmware](https://gitlab.com/efeitosa/seguranca-em-iot/-/blob/main/Aulas/Analise_Firmware.pdf): Apresentação sobre Firmware
- [Bluetooth Low Energy (BLE)](https://gitlab.com/efeitosa/seguranca-em-iot/-/blob/main/Aulas/BLE.pdf): Apresentação sobre BLE
- [Device Fingerprinting](https://gitlab.com/efeitosa/seguranca-em-iot/-/blob/main/Aulas/Device_Fingerprinting.pdf): Apresentação sobre Device Fingerprinting

## 27/04

**Atividade Final - Emulando Firmware com EMUX (27/04)**
~~~
A instalação e configuração do EMUX está disponível em https://github.com/therealsaumil/emux.
Neste site também ficam disponíveis exemplos de como emular um firmware.

A resposta esperada desta ativdade é um relatório apresentando o processo de emulação de dois firmwares já existentes na 
ferramenta e mais dois não existentes. No relatório, mostre telas, comandos e até mesmo erros e falhas encontradas. 
O relatório deve estar em PDF e ser enviado para o email do professor (efeitosa@icomp.ufam.edu.br). Junto com o relatório, 
envie os dois firmwares não existentes no EMUX.

Lembrete: Este trabalho pode ser feito em dupla!!!
Entrega: 12/05 (23h59).
~~~

## Material de Apoio

- [Shodan] (https://www.shodan.io/) - Primeiro mecanismo de busca do mundo para dispositivos conectados à Internet
- [Censys] (https://search.censys.io/) 
- [IoT Inspector 2] (https://inspector.engineering.nyu.edu/)
- [Exploitee.rs] (https://exploitee.rs/) - Lista de dispositivos IoT
- [Hack All The Things: 20 Devices in 45 Minutes] (https://www.youtube.com/watch?v=h5PRvBpLuJs) - Apresentação na DefCon sobre como haquear 20 dispositivos em 45 minutos
- [IoT Attack Surface] (https://documents.trendmicro.com/images/TEx/infographics/The-IoT-Attack-Surface-Infographic.jpg) - Tipos de Superficies de Ataque IoT
- [BluetoothLE] (https://github.com/tigoe/BluetoothLE-Examples) - Repositório contendo exemplos para Bluetooth LE em várias plataformas


## Atividades

1. **Descoberta de dispositivos IoT (13/04)** 
~~~
O objetivo é identificar pelo menos 10 dispositivos IoT, acessíveis publicamente na Internet, 
e relatar o maior quantidade de informações encontradas sobre eles. 
~~~

2. **Análise de Tráfego IoT (13/04)** 
~~~
O objetivo é identificar o padrão de comunicacão de pelo menos 10 dispositivos, informando origem 
(Mac ou IP, e porta), destino (Mac ou IP, e porta) e procotolo (TCP ou UDP). Seguir a planilha 
modelo (https://gitlab.com/efeitosa/seguranca-em-iot/-/blob/main/Planilha_Trace_IoT.xlsx?ref_type=heads) 
e o trace (arquivo .cap) disponibilizado (https://drive.google.com/file/d/1-rCj1tijfhgAcotyXnowolXNdW6V-lcJ/view). 
Usar a ferramenta wireshark. (13/04)
~~~

3. **ARP Spoofing (20/04)**
~~~
Utilizando a ferramenta IoT Inspector 2 (https://inspector.engineering.nyu.edu/), enviar prints dos dispositivos 
descobertos e, se possível, tráfego de rede e endpoints de comunicação. (20/04)
~~~

4. **Analisando o Conteúdo de um Firmware - Parte 1 (20/04)**
~~~
A primeira parte da atividade envolvendo FIRMWARE se caracteriza pela descoberta de sua estrutura. Dos arquivos de firwmware 
disponibilizados (repositório Firmware), vocês terão que realizar uma análise inicial e informar, para cada um, os elementos 
que compõem o firmware (header, bootloader, file system, etc.).

Usem a ferramenta binwalk - o comando é simplesmente binwalk "nome_do_firmware" (sem as aspas)

A resposta para a atividade deve conter o nome do arquivo de firmware analisado, o nome do produto ao qual ele está 
relacionado  (faz parte) e as informações de sua composição.

_Exemplo_:
Arquivo ABC.bin
Produto/Fabricante: Access Point / TNT software
Bootloader: uboot
Header: xxxx
File system: JFFS2 
Kernel: utiliza compressão LZMA ...... 
Outras informações: abc

DICA: Nem todas as informações estão presentes nos arquivos ou são exibidas

DICA: Quanto mais completa a descrição melhor. NÃO quero print dos resultados encontrados pela ferramenta binwalk, mas sim 
o entendimento deles!
~~~

5. **Nossa versão do MIRAI? - Parte 2 (20/04)**
~~~
Como vimos na aula sobre ameaças, o malware MIRAI atacou um grande número de dispositivos IoT ao redor do mundo em 2016. 
Como já dizia o dito popular "o seguro morreu de velho", será que ainda podemos usar as formas de ataque dele (Mirai) 
hoje em dia?

Usando os arquivos de firmware, da primeira parte da atividade - os mais completos e que apresentem file system, vamos 
tentar encontrar vestígios de protocolos legados (telnet) e dos arquivos de login e senha. Usando o comando do linux dd 
e novamente o binwalk, vamos montar e extrair o firmware, paa depois investigar até encontrar dados valiosos. 

A resposta para a atividade (parte 2) deve conter o nome do arquivo de firmware analisado e as provas encontradas:

- arquivos;
- trecho de código; 
- qualquer indicação.

DICA: Execute o binwalk (exemplo: binwalk DAP1353B-firmware-v316-rc015.bin) para descobrir a localização do file system. 
A saída conterá essa informação.
~~~

6. **Emulando Firmware - Parte 3 (20/04)**
~~~
A parte final da atividade 3 consiste na emulação de firmware.

Usando a ferramenta firmadyne (disponível na VM AttifyOS), vamos emular os dois firmware disponizados no repositoório 
(dir300_v1.05_a319.bin e wnap320.zip). 

A resposta esperada desta ativdade são as telas (print) dos dispositivos emulados, MAS com ao menos cinco (5) alterações 
em qualquer parte da configuração. Assim, vocês deverão enviar o print original da característica/configuração e o print 
da alteração.

DICA: use a vm AttifyOS (https://github.com/AttifyOS/AttifyOS). Nela, no diretório /home/oit/tools, vocês encontrarão a 
ferramenta firmadyne. Use o script fat.py que existe nessa ferramenta para emular o firmware.

Lembrete: a senha que o fat.py pede é sempre firmadyne
~~~

7. **Detectando dispositivos BLE - Atividade Opcional**
~~~
Descrição da atividade (https://gitlab.com/efeitosa/seguranca-em-iot/-/blob/main/Atividade_Opcional_BLE.pdf)
~~~